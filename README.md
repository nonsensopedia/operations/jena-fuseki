# Jena Fuseki

Apache Jena Fuseki Docker image

## Usage
- Simply run the container. You can pass in additional arguments that will be forwarded to Fuseki.
- The container listens on port 3030 by default. 
- You can set `JVM_ARGS` to setup the JVM.
- Worth mounting might be:
    - `/fuseki/run/config.ttl`
    - `/fuseki/run/shiro.ini`
    - `/fuseki/run/configuration`
    - `/fuseki/run/databases`
    - `/fuseki/run/backups`
    - `/fuseki/run/logs`
