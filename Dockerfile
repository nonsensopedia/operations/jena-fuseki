FROM openjdk:16-buster

WORKDIR /fuseki

RUN set -eux ; \
    wget https://downloads.apache.org/jena/binaries/apache-jena-fuseki-4.6.1.tar.gz -O fuseki.tar.gz ; \
    tar -zxf fuseki.tar.gz --strip=1 ; \
    rm fuseki.tar.gz

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
